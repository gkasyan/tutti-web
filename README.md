# Test engineer challenge web test #


### About ###

The project uses Selenium Webdriver and Groovy ecosystem: Gradle, Geb, Spockframework

Geb is a Webdriver framework/wrapper, uses the popular page object pattern and adds a concept of a module.
Of course, there are many others ways for web site test automation, not involving Groovy, with Selenium or not, with other frameworks, unit or BDD...
I picked this solution up for demonstration purpose. It has some unique concepts, worth your attention.
I believe this is one of the best and fastest ways to build test automation with Selenium for complex projects.
I have a lot of experience with this approach.


### How do I get set up? ###

Setup a Selenium grid using the "grid" folder provided on a Windows machine.
There are 2 drivers in the "drivers.zip" file, please unzip into the folder.
Run startHub.bat, then startNode.bat
Selenium grid is running now.
Or simply run it in local mode. Download the latest Selenium chromediver(or take it from "drivers.zip" file) and place it into a folder, included into PATH system variable

Setup run configuration in "config/tutti.groovy"
browserName=chrome/firefox/ie
localRun = true/false (false means remote/grid)
seleniumURL=Grid Hub address, local by default

To run the test, run "gradlew test" on Windows or "gradle test" on Linux/OS X
I have tested it on the latest Chrome, Firefox and IE 11 on Windows 8.

src/test/groovy/com/tutti/ta/InsertAnAdSpec.groovy is the test case implementation(specification)
There are some test data at the top of the file, feel free to change it.
Make sure the list of images to upload has correct file paths.
def imagesPaths= ["C:\\Windows\\System32\\GWX\\Download\\resources\\images\\AUX_Info_Bg2.png",
                  "C:\\Windows\\System32\\GWX\\Download\\resources\\images\\home_video.png"]

The specification code is self-documented.

I recommend Intellij IDEA for better Gradle/Groovy support.


### Notes ###

"report" folder contains 2 types of reports already generated.
"screenshots" folder contain page snapshot and screenshot in case of a test failure
Image uploaded tested on all 3 browsers on the grid, local Chrome and Firefox. The functionality does not work on local Firefox run.