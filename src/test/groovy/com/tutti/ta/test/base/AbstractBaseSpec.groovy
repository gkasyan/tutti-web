package com.tutti.ta.test.base

import com.tutti.ta.util.Global
import geb.driver.CachingDriverFactory
import org.junit.Rule
import org.junit.rules.TestName
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.*
import geb.*

abstract class AbstractBaseSpec extends Specification {
    public final static Logger logger = LoggerFactory.getLogger(this.class.name)
    static String gebConfScript = 'GebConfig.groovy'
    @Shared Browser _browser

    public static String gebConfEnv = 'chrome'

    @Rule TestName specTestName

    Configuration createConf() {
        try {
            new ConfigurationLoader(gebConfEnv, System.properties, new GroovyClassLoader(getClass().classLoader)).getConf(gebConfScript)
        } catch (ex) {
            println ex
        }
    }

    protected Browser createBrowser() {
        Configuration gebConfig = createConf()
        gebConfig.baseUrl = Global.config.baseURL
        new Browser(gebConfig)
    }

    Browser getB() {
        if (_browser == null) {
            _browser = createBrowser()
            Global.instance.browser = _browser
        }
        _browser
    }

    void resetBrowser() {
        CachingDriverFactory.clearCacheAndQuitDriver()
        _browser.config.setDriver(null)
        sleep 5000
    }

    def methodMissing(String name, args) {
        getB()."$name"(*args)
    }

    def propertyMissing(String name) {
        getB()."$name"
    }

    def propertyMissing(String name, value) {
        getB()."$name" = value
    }

    def setupSpec() {
        System.setProperty('taf.localRun', (String)Global.config['localRun'])
        System.setProperty('taf.seleniumURL', (String)Global.config['seleniumURL'])
        gebConfEnv = (String)Global.config['browserName']
    }

}
