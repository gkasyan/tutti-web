package com.tutti.ta.test

import com.tutti.ta.gui.pages.ConfirmationPage
import com.tutti.ta.gui.pages.HomePage
import com.tutti.ta.gui.pages.InsertAdPage
import com.tutti.ta.gui.pages.PreviewPage
import com.tutti.ta.test.base.AbstractBaseSpec
import com.tutti.ta.util.FileUploader
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebElement

class InsertAnAdSpec extends AbstractBaseSpec {
    int rentPrice = 5000
    def adSubject = "Test subject 1"
    def adBody = "Test body 1"
    def imagesPaths= ["C:\\Windows\\System32\\GWX\\Download\\resources\\images\\AUX_Info_Bg2.png",
                      "C:\\Windows\\System32\\GWX\\Download\\resources\\images\\home_video.png"]

    def "Insert a rent property ad"() {
        setup: 'Start application'
        to HomePage

        when: 'Click on insert ad'
        insertAd.click()

        then: 'On the Insert Ad page'
        def insertAdPage = b.at InsertAdPage

        when: 'Insert the ad data'
        insertAdPage.with {
            //category = Categories.IMMOBILIEN.value
            category = "Immobilien"
            subCategory = "Wohnungen"
            typeSelection('Ich vermiete eine Wohnung').click()

            size = 100
            rooms = "3.5"
            price = rentPrice
            canton = "Zürich"
            area = "Winterthur"
            zipcode = "8400"
            subject = adSubject
            body = adBody

            email = 'qa-tester@tutti.ch'
            waitFor('tensec') { userName.displayed }
            userName = 'Tester 1'
            phone = "0123456789"
            createAccount.uncheck()

            imagesPaths.each { uploadImage(it)}

            preview.click()
        }

        then: 'at  Preview page data corresponds to inserted:'
        at PreviewPage
        and: 'subject'
        subject.text() == adSubject
        and: 'body'
        body.text() == adBody
        and: 'price'
        def adPrice = price.text().findAll(/\d+/).join().toInteger()
        assert adPrice == rentPrice

        when: "add the ad"
        freeAd.click()
        createButton.click()

        then: "confirmation page displayed"
        at ConfirmationPage
        header.text() == 'Ihr Inserat wurde erfolgreich registriert.'

    }

}
