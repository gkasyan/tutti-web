package com.tutti.ta.util

import geb.navigator.Navigator
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebElement;


class FileUploader {
    def static upload(Navigator fileNavigator, String filePath) {
        RemoteWebElement fileUpload = (RemoteWebElement) fileNavigator.firstElement()
        if (Global.config['browserName'].toString().contains('chrome') || !Global.config['localRun'].toString().toBoolean())
            fileUpload.setFileDetector(new LocalFileDetector())
        fileUpload.sendKeys(filePath)
    }
}
