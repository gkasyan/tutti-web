package com.tutti.ta.util.extensions

import com.tutti.ta.util.Global
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import org.spockframework.runtime.AbstractRunListener
import org.spockframework.runtime.model.ErrorInfo
import org.spockframework.runtime.model.FeatureInfo
import org.spockframework.runtime.model.IterationInfo
import org.spockframework.runtime.model.SpecInfo

class TestRunListener  extends AbstractRunListener {
    final Logger logger = LoggerFactory.getLogger(this.class.name)
    String iterationName

    @Override
    void beforeSpec(SpecInfo spec) {
        MDC.put('testSuite', spec.description.toString())
        MDC.put('testName', '')
        logger.info("Starting specification {}", spec.description.toString())
    }

    @Override
    void beforeFeature(FeatureInfo feature) {
    }

    @Override
    void beforeIteration(IterationInfo iteration) {
        iterationName = iteration.name
    }

    @Override
    void afterIteration(IterationInfo iteration) {
    }

    @Override
    void afterFeature(FeatureInfo feature) {
    }

    @Override
    void afterSpec(SpecInfo spec) {
    }

    @Override
    void error(ErrorInfo error) {
        logger.error(error.exception.message)
        Global.instance?.browser.report iterationName
        sleep 500
    }

    @Override
    void specSkipped(SpecInfo spec) {
    }

    @Override
    void featureSkipped(FeatureInfo feature) {
    }
}
