package com.tutti.ta.util

import geb.Browser

@Singleton
class Global {
    public static ConfigObject config = createConfig()
    public Browser browser

    private static ConfigObject createConfig() {
        def env = System.properties
        String fileName = env['taf.config'] ?: 'config/tutti.groovy'

        ConfigObject config = null;
        try {
            config = new ConfigSlurper().parse(new File(fileName).toURI().toURL())
        } catch (ex) {
            System.err.println "Problem loading the config file " + fileName + ". " + ex.message
        }

        env.each { key, value ->
            if (key.startsWith('taf.')) {
                def configKey = key.replace('taf.', '')
                config[configKey] = value
                if (value == 'false')
                    config[configKey] = false
                if (value == 'true')
                    config[configKey] = true
            }
        }

        config
    }
}