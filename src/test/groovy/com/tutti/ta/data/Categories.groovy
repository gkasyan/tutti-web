package com.tutti.ta.data

/**
 * Created by Gena on 29/11/2015.
 */
enum Categories {
    IMMOBILIEN("1000")

    //private final String value
    String value

    Categories(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
