package com.tutti.ta.gui.pages

import com.tutti.ta.util.FileUploader
import geb.module.Checkbox

class InsertAdPage extends BasePage {
    static atCheckWaiting = 'halfMinute'
    static url =  'ai/form/1'
    static at = {
        form.displayed
    }

    static content = {
        insertAd { $("div#header>a.btn.btn-lifting.btn-ai.btn-size-l.btn-index") }
        form { $("form#ai-form") }
        category { form.$(name: "category_group") }
        subCategory (wait:true) { form.$(name: "sub_category") }
        typeSelection (wait:true) { String labelText ->
                def inputFor = $("div.radio-holder label.line", text: labelText).@for;
                $("input#$inputFor")
        }
        size (wait:true) { $("input#size") }
        rooms (wait:true) { form.$(name: "rooms") }
        price { $("input#price_tb") }
        canton { form.$(name: "region") }
        area (wait:true) { form.$(name: "area") }
        zipcode (wait:true) { $("input#zipcode") }
        subject { $("input#subject") }
        body { $("textarea#body") }
        email { $("input#email") }
        userName (wait:true)  { $("input#name") }
        phone (wait:true)  { $("input#phone") }
        createAccount (wait:true)  { $("input.checkbox", name:'create_account').module(Checkbox) }
        chooseFiles { $("input#file") }
        loadedImages(required:false) { $("div.img-box>img") }
        preview { $("div.form-rules button.reset_button") }
    }

    def uploadImage(String imagePath) {
        def currentNumberOfLoadedImages = loadedImages.size()
        FileUploader.upload(chooseFiles, imagePath)
        waitFor('tensec') {loadedImages.size() > currentNumberOfLoadedImages}
    }

}
