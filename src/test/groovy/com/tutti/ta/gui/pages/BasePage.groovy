package com.tutti.ta.gui.pages

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import geb.Page

class BasePage extends Page {
    public final static Logger logger = LoggerFactory.getLogger(this.class.name)

}
