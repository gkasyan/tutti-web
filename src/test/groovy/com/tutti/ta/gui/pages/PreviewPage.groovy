package com.tutti.ta.gui.pages

class PreviewPage extends BasePage {
    static atCheckWaiting = 'halfMinute'
    static url =  'ai/preview/8'
    static at = {
        subject.displayed
    }

    static content = {
        price { $("li.ai-preview span.in-price>strong") }
        subject { $("div.in-info>h3.in-title-ai") }
        body { $("div.in-info>p.in-text") }
        freeAd { $("input#aipay-free") }
        createButton { $("span#ai_free_btn_holder>button.submit", name:'create') }
    }

}