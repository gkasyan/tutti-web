package com.tutti.ta.gui.pages

class ConfirmationPage extends BasePage {
    static atCheckWaiting = 'halfMinute'
    static url =  'ai/confirm/0'
    static at = {
        header.displayed
    }

    static content = {
        header { $("div.post-info>h2") }
    }

}
