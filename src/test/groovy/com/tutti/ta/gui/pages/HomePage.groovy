package com.tutti.ta.gui.pages

class HomePage extends BasePage {
    static atCheckWaiting = 'halfMinute'
    static url =  ''
    static at = {
        insertAd.displayed
    }

    static content = {
        insertAd { $("div#header>a.btn.btn-lifting.btn-ai.btn-size-l.btn-index") }
    }

}
