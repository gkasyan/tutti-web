/*
	This is the Geb configuration file.
	See: http://www.gebish.org/manual/current/configuration.html
*/


import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.remote.*
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.firefox.*
import org.openqa.selenium.*

import java.util.concurrent.TimeUnit

waiting {
    // all values are in seconds
    timeout = 4
    retryInterval = 0.3

    presets {
        aMinute {
            timeout = 60
            retryInterval = 0.5
        }
        halfMinute {
            timeout = 30
            retryInterval = 0.5
        }
        tensec {
            timeout = 10
        }
        veryLong {
            timeout = 180
        }
    }
}

environments {
    def resize = { wd ->
        wd.manage().window().maximize()
    }
    def setTimeouts = { wd ->
        wd.manage().timeouts().pageLoadTimeout(90, TimeUnit.SECONDS)
        wd.manage().timeouts().setScriptTimeout(180, TimeUnit.SECONDS)
    }

    chrome {
        ChromeOptions options = new ChromeOptions()
        //options.addArguments("--silent");
        driver = {
            def wd
            if (!System.getProperty("taf.localRun").toBoolean()) {
                DesiredCapabilities capabilities = DesiredCapabilities.chrome()
                capabilities.setCapability("chrome.switches", Arrays.asList("--silent"));
                wd = new RemoteWebDriver(new URL((String) System.getProperty("taf.seleniumURL")), capabilities)
            }   else {
                wd = new ChromeDriver(options)
            }
            resize wd
            setTimeouts wd
            wd
        }
    }

    chromeDebug {
        ChromeOptions options = new ChromeOptions()
        options.setExperimentalOption("debuggerAddress", "127.0.0.1:9222")
        //options.addArguments("--silent");
        driver = {
            def wd = new ChromeDriver(options)
            setTimeouts wd
            wd
        }
    }

    firefox {
        driver = {
            def wd
            if (!System.getProperty("taf.localRun").toBoolean()) {
                DesiredCapabilities capabilities = DesiredCapabilities.firefox()
                wd = new RemoteWebDriver(new URL((String) System.getProperty("taf.seleniumURL")), capabilities)
            } else {
                wd = new FirefoxDriver()
            }
            resize wd
            setTimeouts wd
            wd
        }
    }

    ie {
        driver = {
            def wd
            if (!System.getProperty("taf.localRun").toBoolean()) {
                DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer()
                //capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true)
                //capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true)
                capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false)
                capabilities.setCapability(InternetExplorerDriver.SILENT, true)
                capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true)

                wd = new RemoteWebDriver(new URL((String) System.getProperty("taf.seleniumURL")), capabilities)
            } else {
                wd = new InternetExplorerDriver()
            }
            resize wd
            setTimeouts wd
            wd
        }
    }
}

//reporter = new ScreenshotReporter()

reportsDir = "screenshots"
baseNavigatorWaiting = true
atCheckWaiting = 'tensec'
//cacheDriver = false
